# Alastair H. 29th January 2016
# Cleaned up Georgia M 12 March 2018

#######################################
from guardian import GuardState, GuardStateDecorator
from numpy import array, zeros, vstack, savetxt

import time

#import isclib.tcs_flipperscript as tcs_flipperscript

#######################################
#nominal = 'LASER_UP'
nominal = 'DOWN'
request = nominal

#######################################
def find_lock_point(voltage, power):
    slope = zeros(19)

    for i in range(0,19):
        slope[i]=(power[i]-power[i+1])/(voltage[i]-voltage[i+1])
    data = vstack((voltage,power))
    #savetxt('$home/alastair.heptonstall/Data/laser_lock_data.txt',data)
    slope_check = 0
    mid_slope = 0
    counter_pos = 0
    counter_neg = 0

    #JCB 2024/03/28 Changed to ints for midslope
    #check for and find mid-location of region where slope is positive across a range of 24.5V (between 8 measured points)
    for i in range(0,19):
        if slope[i] > 0:
            counter_pos += 1
            counter_neg = 0
        elif slope[i] < 0:
            counter_neg +=1
            counter_pos = 0
        else:
            counter_pos = 0
            counter_neg = 0
        if counter_pos >=8:
            slope_check=1
            mid_slope=int(abs(i-(counter_pos/2)+1))
        if counter_neg >=8:
            slope_check=2
            mid_slope=int(abs(i-(counter_neg/2)+1))

    if slope_check==1:
        log('slopecheck, positive slope {}'.format(slope_check))
        log('midslope matrix index {}'.format(mid_slope))
        log('midvoltage {}'.format(voltage[mid_slope]))
        log('slope {}'.format(slope[mid_slope]))
    elif slope_check==2:
        log('slopecheck, negative slope {}'.format(slope_check))
        log('midslope matrix index {}'.format(mid_slope))
        log('midvoltage {}'.format(voltage[mid_slope]))
        log('slope {}'.format(slope[mid_slope]))
    else:
        log('slopecheckfail {}'.format(slope_check))
    log('slope {}'.format(slope))
    volt_set = voltage[mid_slope]
    power_set = power[mid_slope]
    slope_set = slope[mid_slope]
    return slope_check, volt_set , power_set , slope_set


##################################################
# STATES: INIT / assert_laser_interlock / assert_chiller_temp_mon
##################################################


class INIT(GuardStateDecorator):
    def main(self):
    # Add section here to test for laser state
        pass

    def run(self):
        return True


# Here we will add the guard state decorator that is going to check for an interlock error
class assert_laser_interlock(GuardStateDecorator):
    pass
    #if ezca['LASER_INTERLOCK'] != 0:


class assert_chiller_temp_mon(GuardStateDecorator):
    def pre_exec(self):
        current_chiller_temp = ezca['CHILLER_OUT_GAIN_INMON']
        if current_chiller_temp >=21.5 or current_chiller_temp<=18.5:
            notify('Laser chiller is more than +/-1.5C from 20C. At +/-2C Guardian will return to DOWN state and chiller will reset to 20C')
        if current_chiller_temp >=22 or current_chiller_temp<=18:
            return 'DOWN'
        return


##################################################
# STATES: DOWN
##################################################


class DOWN(GuardState):
    goto=True
    @assert_laser_interlock
    #We aren't doing @assert_chiller_temp_mon here because we already check chiller setpoint in DOWN and this sets up a loop between DOWN and assert_chiller_temp_mon
    def main(self):
        current_chiller_temp=ezca['CHILLER_OUT_GAIN_INMON']
        ezca.switch('PZT_SERVO_GAIN','OUTPUT','OFF')
        ezca['MTRX_1_1']=1
        ezca.switch('PZT_SERVO_GAIN','FM1','OFF')
        if current_chiller_temp >=18.7 and current_chiller_temp <=20.3:# If chiller temp drifts too far it gets reset in the DOWN state. This will require ~30mins to come back to thermal equilibrium so we want to avoid this if possible
            ezca['CHILLER_SET_POINT_OFFSET']=current_chiller_temp
        else:
            notify('Chiller is more than +/-1.3C away from 20C. Resetting to 20C')
            ezca['CHILLER_SET_POINT_OFFSET']=20
        ezca.switch('CHILLER_SERVO_GAIN','INPUT','FM1','FM2','OFF')
        ezca.switch('PZT_SET_POINT','OFFSET','OUTPUT','ON')
        ezca.switch('CHILLER_SET_POINT','OFFSET','OUTPUT','ON')
        ezca['PZT_SET_POINT_OFFSET']=35

    def run(self):
        return True


##################################################
# STATE: FIND_LOCK_POINT
##################################################
# This class will scan the laser cavity PZT and map the power output.  It calls the function 'find_lock_point' which calculates the slope, ensures region with that slope is large enough, and finds the mid-point of the slope to lock to.  If it can't find a suitable locking point then the chiller is stepped in temperature and the process is repeated.

class FIND_LOCK_POINT(GuardState):

    @assert_laser_interlock
    @assert_chiller_temp_mon

    def main(self):
        ezca.switch('PZT_SERVO_GAIN','OUTPUT','OFF')
        ezca['MTRX_1_1']=1
        ezca.switch('PZT_SERVO_GAIN','FM1','OFF')
        ezca.switch('CHILLER_SERVO_GAIN','INPUT','FM1','OFF')
        ezca.switch('PZT_SET_POINT','OFFSET','OUTPUT','ON')

        self.pzt_voltage = zeros(20)
        self.power = zeros(20)
        self.timer_set = False
        self.ii = 0
        self.chiller_timer_set = False
        self.stabilize_timer_set = False

    @assert_laser_interlock
    @assert_chiller_temp_mon

    def run(self):
        if self.ii<=19:
            if self.chiller_timer_set:
                if not self.timer['chillerstep']:
                    return

            self.chiller_timer_set = False

            if not self.timer_set:
                self.pzt_voltage[self.ii] = (self.ii+1)*3.5 # set voltage
                ezca['PZT_SET_POINT_OFFSET'] = self.pzt_voltage[self.ii]
                self.timer['wait'] = 3 # timer for power meter
                self.timer_set = True # set the timer checker

            if not self.timer['wait']:
                return

            self.timer_set = False # reset timer
            self.power[self.ii] = ezca['LSRPWR_HD_PD_OUTPUT']	# measure power

        if self.ii>19:
            if self.stabilize_timer_set:
                if not self.timer['stabilize']:
                    return

                power_at_new_setpoint=ezca['LSRPWR_HD_PD_OUTPUT']
                log('the actual power at the new pzt setpoint is {}'.format(power_at_new_setpoint))

                self.stabilize_timer_set = False

                ezca['LSRPWR_SET_POINT_OFFSET'] = power_at_new_setpoint # reset the setpoint for the laser power to what the power actually is at this pzt voltage.  This stops an offset the moment the servo is turned on.
                return True

            log(', '.join([str(x) for x in self.pzt_voltage]))
            log(', '.join([str(x) for x in self.power]))

            pztlockresult, voltage_setpoint, power_setpoint , slopeset = find_lock_point(self.pzt_voltage,self.power)#calls the function to calculate the locking point

            gainset=1/slopeset

            log('gain setting for servo is {}'.format(gainset))
            log(pztlockresult)
            log(voltage_setpoint)
            log(power_setpoint)

            if pztlockresult == 1 or pztlockresult ==2:
                ezca['PZT_SET_POINT_OFFSET'] = voltage_setpoint
                ezca['LASERPOWER_SETPOINT'] = power_setpoint # store power setpoint in LASERPOWER_SETPOINT channel
                ezca['LSRPWR_SET_POINT_OFFSET'] = power_setpoint # sets the error point for locking laser
                ezca['PZT_SERVO_GAIN_GAIN'] = -gainset #sets the servo gain to one over the laser response in W/V. Note that there seems to have been a negative included somewhere that requires this set as -gainset

                if pztlockresult ==1:
                    ezca['MTRX_1_1']=1 # positive slope set
                else:
                    ezca['MTRX_1_1']=1 # the negative slope is now set in the gain, not in the matrix
                log('laser setpoint chosen')
                self.timer['stabilize']=7
                self.stabilize_timer_set = True
                return

            else:
            #    If no locking point found scanning PZT then : chiller step, set a timer, set a timer checker, set T_step to current GPS time
                if ezca['CHILLER_SET_POINT_OFFSET'] >= 21:
                    ezca['CHILLER_SET_POINT_OFFSET'] = 20.0
                    log('reseting chiller setpoint to 20C')

                else:
                    chiller_current = ezca['CHILLER_SET_POINT_OFFSET']
                    log('current chiller setpoint {}'.format(chiller_current))
                    chiller_current+= 0.1 #increment temperature setpoint.  can be as low as 0.3, but set higher here to speed up cavity length scan
                    ezca['CHILLER_SET_POINT_OFFSET'] = chiller_current
                    ezca['CHILLER_TSTEP'] = ezca[':DAQ-DC0_GPS'] #Set TSTEP to the time of chiller step
                    log('new chiller setpoint {}'.format(chiller_current))
                self.chiller_timer_set = True #set chiller timer
                self.timer['chillerstep'] = 240
                log('no setpoint found:stepping chiller temp')
                self.ii = 0
                return
        else:
            self.ii += 1
            return


##################################################
# STATES: LOCK LASER / RESET_PZT_VOLTAGE
##################################################


# This will connect the laser PZT through a matrix to the thermopile channel, and switches on the filter banks to close the servo loop
class LOCK_LASER(GuardState):

    @assert_laser_interlock
    @assert_chiller_temp_mon

    def main(self):
        ezca.switch('LSRPWR_SET_POINT', 'OFFSET', 'OUTPUT', 'ON') #Laser power setpoint has been set in previous state, so just turn on this offset here
        ezca.switch('PZT_SERVO_GAIN', 'FM1', 'OUTPUT', 'ON') #Turn on the integrator

    @assert_laser_interlock
    @assert_chiller_temp_mon

    def run(self): 
        return True


class RESET_PZT_VOLTAGE(GuardState):

    @assert_laser_interlock
    @assert_chiller_temp_mon

    def main(self):
        self.pztNow = ezca['PZT_SET_POINT_OFFSET']
        self.pztMid = 35
        if self.pztMid != self.pztNow:
            self.dV = (self.pztMid - self.pztNow)/64
            #This class will offload the PZT offset voltage from the PZT_SET_POINT_OFFSET to the servo which has an integrator by slowly moving the offset channel back to zero, allowing the integrator to build up a DC offset
    # step through the intermediate PZT voltages over 64 steps at 16Hz.

    @assert_laser_interlock
    @assert_chiller_temp_mon

    def run(self):
        if abs(self.pztNow - self.pztMid) <=0.05:
            return True
        self.pztNow += self.dV
        ezca['PZT_SET_POINT_OFFSET'] = self.pztNow
        self.timer['wait'] = 0.1 #wait for 0.1 seconds so that hand off time takes about 6.5 seconds.
        return


##################################################
# STATES: ENGAGE_CHILLER_SERVO / ISS_ENGAGE
##################################################


#This class turns on a servo loop that will move the PZT back to the center of its range (around 35V) by servo-ing the chiller temperature.  This should just involve turning on a preset filter bank
class ENGAGE_CHILLER_SERVO(GuardState):

    @assert_laser_interlock
    @assert_chiller_temp_mon

    def main(self):
        # set up the filter switches and gains
        # basically this ensures the correct filter and switch values overriding manual changes
        # turn on filter module ezca.switch('CHILLER_SERVO_GAIN','FM1','ON')
        # gain of lasers is 1.25mK/V - needs a UGF around 0.2mHz
        # ezca['CHILLER_SERVO_GAIN_GAIN'] = 1.25E-3

        ezca['MTRX_2_1'] = 1
        ezca.switch('CHILLER_SERVO_GAIN','INPUT','FM1','FM2','OUTPUT','ON')
        ezca.switch('CHILLER_OUT_GAIN','INPUT','OUTPUT','ON')

    @assert_laser_interlock
    @assert_chiller_temp_mon

    def run(self):
        return True

#We won't be using the ISS immediately, but this will essentially just turn on switch CO2_ISS_LOOP_SW_SW2 to close an analogue electronics loop
class ISS_ENGAGE(GuardState):

    @assert_laser_interlock
    @assert_chiller_temp_mon

    def main(self):
        #set the output voltage for ISS servo switch
        #ezca['ISS_LOOP_SW_OFFSET'] = 32000
        #engage ISS servo - this is commented out 2/5/2015 to initially disable the ISS circuit AWH
        #ezca.switch('ISS_LOOP_SW','OFFSET','OUTPUT','ON')
        pass

    @assert_laser_interlock
    @assert_chiller_temp_mon

    def run(self):
        # removing check that ISS servo has been turned on 2/5/2015 AWH
        #if ezca['LOOP_SW_RB_INMON']>16000:
        #	print "ISS LOOP CLOSED"
        return True


##################################################
# STATES: LASER_UP / LASER_INTERLOCK_TRIP
##################################################


# This will just monitor laser power for any excursions and the PZT to check range, then decide whether to send back to DOWN state
class LASER_UP(GuardState):

    @assert_laser_interlock
    @assert_chiller_temp_mon

    def main(self):
        pass

    @assert_laser_interlock
    @assert_chiller_temp_mon

    def run(self):
        current_power=ezca['LSRPWR_HD_PD_OUTPUT']
        setpoint=ezca['LASERPOWER_SETPOINT']
        if current_power <=35:#40:#check whether the laser is actually on
            print('laser appears to be off')
            return 'DOWN'
        elif ezca['PZT_OUT_GAIN_INMON'] >= 70 or ezca['PZT_OUT_GAIN_INMON'] <= 0.05: #Check whether laser PZT is within an acceptable range
            log('laser unlocked. jumping to find new locking point')
            return 'FIND_LOCK_POINT'

        elif abs(current_power-setpoint)>=2: #check laser power hasn't changed beyond a certain range
            log('laser power outside range. Setpoint is {}. Current power is {}. Jumping to find new locking point'.format(setpoint,current_power))
            return 'FIND_LOCK_POINT'
        else:
            return True

class LASER_INTERLOCK_TRIP(GuardState):

    @assert_chiller_temp_mon

    #request=False
    def main(self):
        if interlock == 3:
            notify('TCS CO2 LASER INTERLOCK TRIPPED')



####################################################
# Hacked by Adam Mullavey
####################################################

def rs_stuck_check():
    time.sleep(1.3)
    log('Checking if rotation stage is stuck')
    if ezca['LASERPOWER_RS_EXECUTE'] and not ezca['LASERPOWER_RS_BUSY']:
        log('Rotation stage was stuck, unsticking')
        ezca['LASERPOWER_ABORT'] = 1
        time.sleep(1)


def goto_power(power):
    rs_stuck_check()
    ezca['LASERPOWER_POWER_REQUEST'] = power
    time.sleep(1)
    ezca['LASERPOWER_COMMAND'] = 2

def goto_angle(angle):
    rs_stuck_check()
    ezca['LASERPOWER_ANGLE_REQUEST'] = angle
    time.sleep(1)
    ezca['LASERPOWER_COMMAND'] = 4



class DOWN_BASIC(GuardState):
    index = 1
    request = True
    goto = True

    def main(self):

        #goto_power(0)
        rs_stuck_check()
        ezca['LASERPOWER_COMMAND'] = 3

    def run(self):

        #TODO: check that went to requested power

        return True

class SET_INTERMEDIATE_POWER(GuardState):
    index = 90
    request = True

    def main(self):

        rs_stuck_check()
        goto_angle(intermediate_angle)
        time.sleep(5)

    def run(self):

        return True

#TODO: add some checks and stuff
class SET_FINAL_POWER(GuardState):
    index = 100
    request = True

    def main(self):

        #optic = 
        #tcs_flipperscript.setTCSMask('ITMX',ifoconfig.default_itmx_tcs_mask)  

        #goto_power(final_power)
        rs_stuck_check()
        goto_angle(final_angle)
        time.sleep(8)

        self.counter_exec = 0

    def run(self):

        if self.counter_exec > 30:
            rs_stuck_check()

        #return True # disable for night 2 April 2024
        if ezca['LASERPOWER_RS_EXECUTE'] == 1:
            self.counter_exec += 1
            notify('CO2 power stage rotating')
            time.sleep(1)
            return True

        self.counter_exec = 0

        if abs(ezca['LSRPWR_MTR_OUTPUT'] - final_power) > 0.15:
            notify('Manual intervention required, CO2 power more than 10 percent off')
            return
        elif abs(ezca['LSRPWR_MTR_OUTPUT'] - final_power) > 0.02:
            notify('CO2 power not correct, adjusting rotation stage')
            current_angle = ezca['LASERPOWER_ANGLE_REQUEST']
            if ezca['LSRPWR_MTR_OUTPUT'] - final_power < 0:
                goto_angle(current_angle + 0.05)
            elif ezca['LSRPWR_MTR_OUTPUT'] - final_power > 0:
                goto_angle(current_angle - 0.05)
            time.sleep(4) #JCB changed from 0.3 to 4
            return True
        
        return True


##################################################
# EDGES
##################################################


edges = [
    ('LASER_UP', 'DOWN'),
    ('LASER_INTERLOCK_TRIP', 'DOWN'),
    ('DOWN', 'FIND_LOCK_POINT'),
    ('FIND_LOCK_POINT', 'LOCK_LASER'),
    ('LOCK_LASER', 'RESET_PZT_VOLTAGE'),
    ('RESET_PZT_VOLTAGE', 'ENGAGE_CHILLER_SERVO'),
    ('ENGAGE_CHILLER_SERVO', 'ISS_ENGAGE'),
    ('ISS_ENGAGE', 'LASER_UP'),
    ('DOWN_BASIC','DOWN_BASIC'),
    ('DOWN_BASIC','SET_INTERMEDIATE_POWER'),
    ('SET_INTERMEDIATE_POWER','SET_FINAL_POWER')
    ]

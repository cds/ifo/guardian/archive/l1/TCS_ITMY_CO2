import TCS_CO2

# settings below for 56W
TCS_CO2.intermediate_power = 0.0
TCS_CO2.intermediate_angle = 38.4

TCS_CO2.final_power = 0.0 #1.437
TCS_CO2.final_angle = 38.4 # 52.1

# Setting for intermediate power
#TCS_CO2.intermediate_power = 1.44
#TCS_CO2.intermediate_angle = 52.0


# settings below for 64W
#TCS_CO2.final_power = 1.44 #1.437
#TCS_CO2.final_angle = 52.0 # 52.1

# settings below for 72W 
#TCS_CO2.final_power = 2.41 # 2.05 
#TCS_CO2.final_angle = 56.7 # 55.1

#TCS_CO2.final_power = 1.00
#TCS_CO2.final_angle = 49.6
#TCS_CO2.final_power = 2.2
#TCS_CO2.final_angle = 56.3

from TCS_CO2 import *

prefix = 'TCS-ITMY_CO2'
#request = 'LASER_UP'
request = 'DOWN'
#nominal = 'LASER_UP'
#nominal = 'SET_FINAL_POWER'
nominal = 'DOWN_BASIC'
